using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class AutoSave : EditorWindow
{
    public float saveTime = 300;
    public float nextSave = 0;

    [MenuItem("UsefulScripts/AutoSave")]

    private static void Init()
    {
        AutoSave window = (AutoSave)EditorWindow.GetWindowWithRect(typeof(AutoSave), new Rect (0, 0, 200, 50));
        window.Show();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("Save Each:", saveTime + " Secs");
        float timeToSave = nextSave - (float)EditorApplication.timeSinceStartup;
        EditorGUILayout.LabelField("Next Save:", timeToSave.ToString() + " Sec");
        Repaint();

        if(EditorApplication.timeSinceStartup > nextSave)
        {
            string[] path = EditorSceneManager.GetActiveScene().path.Split(char.Parse("/"));
            path[path.Length - 1] = path[path.Length - 1];
            bool saveOK = EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene(), string.Join("/", path));
            Debug.Log("Saved Scene " + (saveOK ? "OK" : "Error!"));
            nextSave = (float)EditorApplication.timeSinceStartup + saveTime;
        }
    }
}
