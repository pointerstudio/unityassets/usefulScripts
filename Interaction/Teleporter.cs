using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// to be nice to anyone using it,
// we will use some editor functionality, like notifications
// this makes the code a bit bigger than necessary, but hopefully friendlier to work with
#if UNITY_EDITOR
using UnityEditor;
#endif


// This script only works, if the object we attach it to has a collider
[RequireComponent(typeof(Collider))]
public class Teleporter : MonoBehaviour
{
    [Tooltip("Your player or object to be teleported")]
    public GameObject player;

    [Tooltip("Where shall we teleport the player")]
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        // be kind to ourselves and automatically try looking for the player
        // should we have forgotten to drag it in
        if (player == null) {
            player = GameObject.Find("Super First Person Controller");
        }
        // and also look for a gameobject called "Player" just in case
        if (player == null) {
            player = GameObject.Find("Player");
        }
        // we can also show the user some notifications if it didn't find anything yet
        // this hopefully helps with debugging
#if UNITY_EDITOR
        if (player == null) {
            EditorUtility.DisplayDialog("Player not specified", "please drag and drop your player in the Teleporter script of " + gameObject.name, ".. ok");
        }
        if (target == null) {
            EditorUtility.DisplayDialog("Teleportation target not specified", "please drag and drop your teleportation target in the Teleporter script of " + gameObject.name, ".. ok");
        }
#endif
    }

    // in case that we do not have "isTrigger" activated on our Collision
    // we must use OnCollisionEnter to detect if the player collides with it
    void OnCollisionEnter(Collision collision)
    {
        // check if the colliding gameobject is the player
        if (collision.gameObject == player) {
            // yessss, it is. so let's reset it's position
            player.transform.position = target.transform.position;
        }
    }

    // in case that we do not have "isTrigger" activated on our Collision
    // we must use OnCollisionEnter to detect if the player collides with it
    void OnTriggerEnter(Collider collider)
    {
        // check if the colliding gameobject is the player
        // note, that we do not need to do collider.gameObject, because
        // the collider is already a gameObject
        // anyways... is it the player?
        if (collider == player) {
            // yessss, it is. so let's reset it's position
            player.transform.position = target.transform.position;
        }
    }
}
